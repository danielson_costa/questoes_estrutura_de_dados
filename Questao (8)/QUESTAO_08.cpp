#include <iostream>
#include <stdlib.h>

/*8. Implemente o algoritmo de ordena��o selection sort e aplique a um vetor com vinte posi��es. Os
valores do vetor devem ser lidos pelo usu�rio e apresentados, ordenados, na tela do programa*/

using namespace std;

const int tamanho = 20;

int vetor[tamanho];

void lerVetor(int vet[], int tam){
cout<<"========================================\n";
cout << "INFORME OS VALORES PARA O VETOR \n";
cout<<"========================================\n";

    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[], int tam){
     cout<<"======================================================\n";
     cout << "# A posicao do vetor estruturado pelo selection sort #" << endl;
     cout<<"======================================================\n";
     
    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
    }
}

void selectionSort(int vet[], int tam){

    for(int i = 0; i < tam; i++){
        int menor = i;
        for(int j =i+1; j < tam; j++){
            if(vet[j] < vet[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vet[i];
            vet[i] = vet[menor];
            vet[menor] = temp;
        }
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    selectionSort(vetor,tamanho);
    system("cls");
    imprimirVetor(vetor,tamanho);
    
system("PAUSE");
return EXIT_SUCCESS;
}
