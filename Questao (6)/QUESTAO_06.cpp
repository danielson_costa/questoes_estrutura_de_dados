#include <iostream>
#include <stdlib.h>
/*6. Fa�a um programa que receba um vetor com 15 posi��es e fa�a a escrita dos valores ao contr�rio das
posi��es iniciais. Por�m, o programa deve percorrer o vetor e fazer a invers�o usando ponteiros*/
using namespace std;

const int tamanho=15;

int *vetor[tamanho];

void inicializarVetor (int *vet[], int tam){
    for (int i=0; i<tam; i++){
        vet[i] = new int;
        *(vet[i])=0;
    }
}

void lerVetor (int *vet[], int tam){
     cout<<"========================================\n";
     cout << "INFORME 15 VALORES PARA O VETOR: \n";
     cout<<"========================================\n";

    for (int i=0; i<tam; i++){
        cout <<"Entre com o " << (i+1)<<" valor: ";
        cin >> *(vet[i]);
    }
}

void imprimirVetor (int *vet[], int tam){
     cout<<"========================================\n";
     cout << "VETORES DE FORMA CONTRARIA: \n";
     cout<<"========================================\n";
    int **ptro = &vet[tam-1];
    for (int i=tam-1; i>=0; i--){
    cout << "Posi��o: " << (i+1) <<": " << **(ptro)<<endl;
    ptro--;
    }
}

int main()
{
    inicializarVetor(vetor, tamanho);
    lerVetor(vetor, tamanho);
    system("cls");
    imprimirVetor(vetor, tamanho);

  system("PAUSE");
return EXIT_SUCCESS;
}
