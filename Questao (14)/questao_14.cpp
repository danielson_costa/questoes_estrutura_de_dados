#include <iostream>
#include <stdlib.h>


/*14. Fa�a um programa que leia um vetor de 10 posi��es e realize a ordena��o pelo algoritmo quicksort.*/

using namespace std;

const int tamanho=10;

int vetor[tamanho];

void lerVetor(int vetor[],int tamanho){
	cout<<"========================================\n";
      cout << "INFORME OS VALORES PARA O VETOR: \n";
      cout<<"========================================\n";

    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}

void imprimirVetor(int vetor[],int tamanho){
      cout<<"========================================\n";
      cout << "ORDENANDO VETORES COM O QUICK_SORT: \n";
      cout<<"========================================\n";

    for (int i=0; i<tamanho; i++){
        cout<<"Posicao "<<(i+1)<<": "<<vetor[i]<<endl;
    }
}

void qs(int vetor[],int left,int right);

void quicksort(int vetor[],int left,int right){
    qs(vetor,left,right-1);
}

void qs(int vetor[],int left,int right){

    int i,j;
    i=left;
    j=right;
    int meio = vetor[(left+right)/2];

    while (j>i){
        while (vetor[i]<meio && i<right) i++;
        while (meio<vetor[j] && j>left) j--;

        if (i<=j){
            int temp=vetor[i];
            vetor[i]=vetor[j];
            vetor[j]=temp;
            i++;
            j--;
        }
    }

    if (left<j) qs(vetor,left,j);
    if (i<right) qs(vetor,i,right);

}

int main()
{

    lerVetor(vetor,tamanho);
    system("cls");
    quicksort(vetor,0,tamanho);
    imprimirVetor(vetor,tamanho);
    system("PAUSE");
return EXIT_SUCCESS;

}
