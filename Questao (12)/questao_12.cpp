#include <iostream>
#include <stack>
#include <stdlib.h>
/* 12. Fa�a um programa que leia 10 posi��es de caracteres e os imprima em ordem inversa. Este programa deve usar uma pilha*/

void exibirPilhaInvertida(std::stack<int>& x) {
     
	std::cout << "# PILHA INVERTIDA #" << std::endl;

	while (!x.empty()) {
		std::cout << x.top() << " "<<std::endl;
		x.pop();
	}
}

int main() {
	std::stack<int> p;
	int numero = 0;

	std::cout << "# INFORME OS VALORES PARA PILHA #" << std::endl;
	for (int i = 0; i <= 9; i++) {
     std::cout<<"Posicao ";
	std::cin >> numero;
	p.push(numero);
	}
	system("cls");

	exibirPilhaInvertida(p);
    system("PAUSE");
return EXIT_SUCCESS;
}

    
