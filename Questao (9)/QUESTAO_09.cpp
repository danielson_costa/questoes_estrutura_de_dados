#include <iostream>
#include <stdlib.h>
/*9. Desenvolva um programa que ordene um vetor de inteiro com dez posi��es. Os valores devem ser
lidos no terminal. Usar o bublle sort.*/

using namespace std;

const int tamanho = 10;

int vetor[tamanho];

void lerVetor(int vet[],int tam){
     cout<<"========================================\n";
     cout << "INFORME OS VALORES PARA O VETOR \n";
     cout<<"========================================\n";
    for (int i=0;i<tam;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[],int tam){
     cout<<"=================================================\n";
     cout << "A posicao do vetor estruturado pelo bubble sort #" << endl;
     cout<<"=================================================\n";
    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
    }
}

void bubbleSort(int vet[],int tam){
    for(int i=0; i < tam; i++){
        for(int j = tam-1; j >= 1 ;j--){
            if(vet[j-1] > vet[j]){
                int temp = vet[j-1];
                vet[j-1] = vet[j];
                vet[j] = temp;
            }
        }
    }
}

int main()
{
    lerVetor(vetor,tamanho);
    bubbleSort(vetor,tamanho);
    system("cls");
    imprimirVetor(vetor,tamanho);
system("PAUSE");
return EXIT_SUCCESS;

}
