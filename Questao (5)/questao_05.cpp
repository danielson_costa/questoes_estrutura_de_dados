#include <iostream>
#include <stdlib.h>

/*Questao 05. Fazer um sistema que preencha um vetor com cinco registros. A estrutura do registro � apresentada a
seguir:
struct registro {
int codigo;
char nome[60];
char endereco[60];
};*/

using namespace std;
const int tamanho=5;

struct registro {
	int codigo;
	char nome[60];
	char endereco[60];
};

registro *registros[tamanho];

void inicializarEstrutura (registro *reg[], int tam){
    for (int i=0; i<tam; i++){
        reg[i]= new registro;
    }
}

void lerEstrutura(registro *reg[], int tam){
     cout<<"====================";
     cout<<"\n# TELA DE REGISTRO #";
    cout<<"\n====================\n";
    
	for (int i=0; i<tam; i++){
    cout << "Digite seu nome: ";
	cin.getline(reg[i]->nome,60);
	cout << "Digite seu endereco: ";
	cin.getline(reg[i]->endereco,60);
	cout << "Digite seu codigo: ";
	cin >> reg[i]->codigo;
	cout<<endl;
	cin.ignore();
		}
	}



void ordenarPorSelecao(registro *reg[],int tamanho){
    for (int i=0;i<tamanho;i++){
        int min = i;
        int j = i + 1;
        while (j < tamanho){
            if (reg[j]->codigo<reg[min]->codigo){
                min = j;
            }
            j++;
        }
        if (i!=min){
            registro *temp = reg[i];
            reg[i] = reg[min];
            reg[min] = temp;

        }
    }
}

bool pesquisaSequencial(registro *reg[],int alvo){
    for (int i=0;i<tamanho;i++){
        if (reg[i]->codigo==alvo){
            return true;
        }
    }
    return false;
}

int main () {
    inicializarEstrutura(registros, tamanho);
	lerEstrutura(registros,tamanho);
	system("cls");
	
	ordenarPorSelecao(registros, tamanho);
    int alvo;
    cout << "# INFORME O CODIGO QUE DESEJA PESQUISAR:# "<<endl;
    cin>> alvo;
    if (pesquisaSequencial(registros,alvo)) {

        for (int i=0; i<tamanho; i++){
            if (registros[i]->codigo==alvo){
            registros[alvo-1]=registros[i];
            }
        }
        system("cls");
        cout <<"# O CODIGO INFORMADO FOI ENCONTRADO #"<<endl;
        cout <<"Nome: " << registros[alvo-1]->nome<<endl;
        cout <<"Endereo: " << registros[alvo-1]->endereco;
        cout<<"\n";
    } else {
        cout <<"# CODIGO NAO ENCONTRADO #\n";
    }
    system("PAUSE");
return EXIT_SUCCESS;
 }
